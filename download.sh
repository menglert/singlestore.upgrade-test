#!/bin/bash

BASE_URL="https://release.memsql.com"
S2_BASE_FOLDER="/opt/singlestore/"
ARTIFACT_FOLDER="${S2_BASE_FOLDER}artifacts/"
DB_OLD="8.0.32"
TB_OLD="1.17.9"
CL_OLD="1.0.7"
VERSION_NEW="latest"

#Get the New Version
/usr/bin/mkdir -p ${ARTIFACT_FOLDER}${VERSION_NEW}
/usr/bin/mkdir -p ${S2_BASE_FOLDER}singlestore-client/${VERSION_NEW}
/usr/bin/mkdir -p ${S2_BASE_FOLDER}singlestoredb-toolbox/${VERSION_NEW}
/usr/bin/curl -o ${ARTIFACT_FOLDER}${VERSION_NEW}/memsqltoolbox.json ${BASE_URL}/production/index/memsqltoolbox/${VERSION_NEW}.json
/usr/bin/curl -o ${ARTIFACT_FOLDER}${VERSION_NEW}/memsqltoolbox.tar.gz ${BASE_URL}/$(/usr/bin/jq -r '.packages."memsql-toolbox-tar".Path' ${ARTIFACT_FOLDER}${VERSION_NEW}/memsqltoolbox.json)
/usr/bin/echo "$(/usr/bin/jq -r '.packages."memsql-toolbox-tar".Sha256Sum' ${ARTIFACT_FOLDER}${VERSION_NEW}/memsqltoolbox.json) ${ARTIFACT_FOLDER}${VERSION_NEW}/memsqltoolbox.tar.gz" | /usr/bin/sha256sum --check
/usr/bin/tar xvzf ${ARTIFACT_FOLDER}${VERSION_NEW}/memsqltoolbox.tar.gz -C ${S2_BASE_FOLDER}singlestoredb-toolbox/${VERSION_NEW} --strip-components=1
/usr/bin/curl -o ${ARTIFACT_FOLDER}${VERSION_NEW}/memsqlclient.json ${BASE_URL}/production/index/memsqlclient/${VERSION_NEW}.json
/usr/bin/curl -o ${ARTIFACT_FOLDER}${VERSION_NEW}/memsqlclient.tar.gz ${BASE_URL}/$(/usr/bin/jq -r '.packages."memsql-client-tar".Path' ${ARTIFACT_FOLDER}${VERSION_NEW}/memsqlclient.json)
/usr/bin/echo "$(/usr/bin/jq -r '.packages."memsql-client-tar".Sha256Sum' ${ARTIFACT_FOLDER}${VERSION_NEW}/memsqlclient.json) ${ARTIFACT_FOLDER}${VERSION_NEW}/memsqlclient.tar.gz" | /usr/bin/sha256sum --check
/usr/bin/tar xvzf ${ARTIFACT_FOLDER}${VERSION_NEW}/memsqlclient.tar.gz -C ${S2_BASE_FOLDER}singlestore-client/${VERSION_NEW} --strip-components=1
/usr/bin/curl -o ${ARTIFACT_FOLDER}${VERSION_NEW}/singlestoredbserver.json ${BASE_URL}/production/index/singlestoredbserver/${VERSION_NEW}.json
/usr/bin/curl -o ${ARTIFACT_FOLDER}${VERSION_NEW}/singlestoredbserver.tar.gz ${BASE_URL}/$(/usr/bin/jq -r '.packages."singlestoredb-server-tar".Path' ${ARTIFACT_FOLDER}${VERSION_NEW}/singlestoredbserver.json)
/usr/bin/echo "$(/usr/bin/jq -r '.packages."singlestoredb-server-tar".Sha256Sum' ${ARTIFACT_FOLDER}${VERSION_NEW}/singlestoredbserver.json) ${ARTIFACT_FOLDER}${VERSION_NEW}/singlestoredbserver.tar.gz" | /usr/bin/sha256sum --check
/usr/bin/rm -rf ${ARTIFACT_FOLDER}${VERSION_NEW}/*.json

#Get the Old Version
/usr/bin/mkdir -p ${ARTIFACT_FOLDER}old
/usr/bin/mkdir -p ${S2_BASE_FOLDER}singlestore-client/old
/usr/bin/mkdir -p ${S2_BASE_FOLDER}singlestoredb-toolbox/old
/usr/bin/curl -o ${ARTIFACT_FOLDER}old/memsqltoolbox.json ${BASE_URL}/production/index/memsqltoolbox/${TB_OLD}.json
/usr/bin/curl -o ${ARTIFACT_FOLDER}old/memsqltoolbox.tar.gz ${BASE_URL}/$(/usr/bin/jq -r '.packages."memsql-toolbox-tar".Path' ${ARTIFACT_FOLDER}old/memsqltoolbox.json)
/usr/bin/echo "$(/usr/bin/jq -r '.packages."memsql-toolbox-tar".Sha256Sum' ${ARTIFACT_FOLDER}old/memsqltoolbox.json) ${ARTIFACT_FOLDER}old/memsqltoolbox.tar.gz" | /usr/bin/sha256sum --check
/usr/bin/tar xvzf ${ARTIFACT_FOLDER}old/memsqltoolbox.tar.gz -C ${S2_BASE_FOLDER}singlestoredb-toolbox/old --strip-components=1
/usr/bin/curl -o ${ARTIFACT_FOLDER}old/memsqlclient.json ${BASE_URL}/production/index/memsqlclient/${CL_OLD}.json
/usr/bin/curl -o ${ARTIFACT_FOLDER}old/memsqlclient.tar.gz ${BASE_URL}/$(/usr/bin/jq -r '.packages."memsql-client-tar".Path' ${ARTIFACT_FOLDER}old/memsqlclient.json)
/usr/bin/echo "$(/usr/bin/jq -r '.packages."memsql-client-tar".Sha256Sum' ${ARTIFACT_FOLDER}old/memsqlclient.json) ${ARTIFACT_FOLDER}old/memsqlclient.tar.gz" | /usr/bin/sha256sum --check
/usr/bin/tar xvzf ${ARTIFACT_FOLDER}old/memsqlclient.tar.gz -C ${S2_BASE_FOLDER}singlestore-client/old --strip-components=1
/usr/bin/curl -o ${ARTIFACT_FOLDER}old/singlestoredbserver.json ${BASE_URL}/production/index/singlestoredbserver/${DB_OLD}.json
/usr/bin/curl -o ${ARTIFACT_FOLDER}old/singlestoredbserver.tar.gz ${BASE_URL}/$(/usr/bin/jq -r '.packages."singlestoredb-server-tar".Path' ${ARTIFACT_FOLDER}old/singlestoredbserver.json)
/usr/bin/echo "$(/usr/bin/jq -r '.packages."singlestoredb-server-tar".Sha256Sum' ${ARTIFACT_FOLDER}old/singlestoredbserver.json) ${ARTIFACT_FOLDER}old/singlestoredbserver.tar.gz" | /usr/bin/sha256sum --check
/usr/bin/rm -rf ${ARTIFACT_FOLDER}old/*.json

exit 0