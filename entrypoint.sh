#!/bin/bash

/usr/bin/sed -i -e "s/SINGLESTORE_LICENSE/$SINGLESTORE_LICENSE/g" /opt/singlestore/singlestoredb-toolbox/s2_cluster.yaml
/usr/bin/sed -i -e "s/ROOT_PASSWORD/$ROOT_PASSWORD/g" /opt/singlestore/singlestoredb-toolbox/s2_cluster.yaml

/usr/sbin/init

/usr/bin/journalctl -f