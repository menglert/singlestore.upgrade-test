# Testing S2 Updates
## Pre-Requisites
* Docker
* Docker Compose
* (MacOS) Use the Apple Virtualization Framework for emulation
## Setup
* Clone repo `git clone https://gitlab.com/menglert/singlestore.upgrade-test.git`
* Change to repository `cd singlestore.upgrade-test`
* Create a `.env` file based on `env.sample`
  * Set the license and the root password
* Build and run via docker compose `docker-compose up -d --no-deps --build`
  * This will create 3 Containers (1 Master, 2 Leave Nodes)
* Shell into Master Container `docker exec -ti --user s2 singlestoreupgrade-test-s2_master-1 bash`
* Download the bits `./download.sh`
* Go to the *old* Toolbox version `cd singlestoredb-toolbox/old/`
* Deploy the cluster in non interactive mode
```
./sdb-deploy setup-cluster --temp-dir /opt/singlestore/tmp/ --cluster-file ../s2_cluster.yaml --ssh-max-sessions 3 --yes
```
* Wait to finish the installation you should see this
```
✓ Registered hosts
✓ Installed singlestoredb-server-8.0.32-4959d03ded on host upgrades-s2_master-1 (1/3)
✓ Installed singlestoredb-server-8.0.32-4959d03ded on host upgrades-s2_node-1 (2/3)
✓ Installed singlestoredb-server-8.0.32-4959d03ded on host upgrades-s2_node-2 (3/3)
✓ Successfully installed on 3 hosts
✓ No nodes to register
✓ Created master node
✓ Successfully set license
✓ Bootstrapped master aggregator
✓ Created all Nodes
✓ Added leaf nodes to the cluster
✓ No aggregators to add
The Final Cluster State
Hosts
+----------------------+------------+-----------------------+-------------------------+
|         Host         | Local Host |      SSH address      |      Identity File      |
+----------------------+------------+-----------------------+-------------------------+
| upgrades-s2_master-1 | Yes        |                       |                         |
| upgrades-s2_node-1   | No         | s2@upgrades-s2_node-1 | /opt/singlestore/id_rsa |
| upgrades-s2_node-2   | No         | s2@upgrades-s2_node-2 | /opt/singlestore/id_rsa |
+----------------------+------------+-----------------------+-------------------------+
Nodes
+------------+--------+----------------------+------+---------------+--------------+---------+----------------+--------------------+--------------+
| MemSQL ID  |  Role  |         Host         | Port | Process State | Connectable? | Version | Recovery State | Availability Group | Bind Address |
+------------+--------+----------------------+------+---------------+--------------+---------+----------------+--------------------+--------------+
| 182D3F9D62 | Master | upgrades-s2_master-1 | 3306 | Running       | True         | 8.0.32  | Online         |                    | 0.0.0.0      |
| C85CF57998 | Leaf   | upgrades-s2_node-1   | 3306 | Running       | True         | 8.0.32  | Online         | 1                  | 0.0.0.0      |
| CDD5AEEBD0 | Leaf   | upgrades-s2_node-2   | 3306 | Running       | True         | 8.0.32  | Online         | 1                  | 0.0.0.0      |
+------------+--------+----------------------+------+---------------+--------------+---------+----------------+--------------------+--------------+
```
* Now switch to the *latest* toolbox version `cd ../latest/`
* Test the Cluster upgrade
```
./sdb-deploy upgrade --precheck-only --ssh-max-sessions 3 --file-path ../../artifacts/latest/singlestoredbserver.tar.gz --yes
```
* Verify the pre-checks
```
✓ Finished checking SSL certificates
✓ Finished configuring ca-certificates
Checking cluster status
✓ Nodes are online
✓ Partitions are healthy

✓ Snapshots completed
✓ All checks passed successfully
Operation completed successfully
```
* Perform the Cluster Upgrade
```
./sdb-deploy upgrade --ssh-max-sessions 3 --file-path ../../artifacts/latest/singlestoredbserver.tar.gz --yes
```
* Wait and verify that everything worked fine
```
✓ Finished checking SSL certificates
✓ Finished configuring ca-certificates
Checking cluster status
✓ Nodes are online
✓ Partitions are healthy

✓ Snapshots completed
✓ All checks passed successfully
✓ Installed singlestoredb-server-8.5.11-b906f298a2 on host singlestoreupgrade-test-s2_master-1 (1/3)
✓ Installed singlestoredb-server-8.5.11-b906f298a2 on host singlestoreupgrade-test-s2_node-1 (2/3)
✓ Installed singlestoredb-server-8.5.11-b906f298a2 on host singlestoreupgrade-test-s2_node-2 (3/3)
✓ Successfully installed on 3 hosts
The memsqlctl binary now has version 8.5.11-b906f298a2
Updating node configuration:
✓ Set leaf_failure_detection to OFF

✓ Stopped Master node on singlestoreupgrade-test-s2_master-1 (1/1)
✓ Successfully stopped Master node on 1 host
✓ Stopped Master node
✓ Stopped Leaf nodes on singlestoreupgrade-test-s2_node-1 (1/2)
✓ Stopped Leaf nodes on singlestoreupgrade-test-s2_node-2 (2/2)
✓ Successfully stopped Leaf nodes on 2 hosts
✓ Stopped Leaf nodes
✓ Set variables
memsqlctl output:
WARNING: The NICE limit should be at most -10. This system is currently configured with the limit set to 20.
WARNING: To avoid memory errors, vm.max_map_count should be at least 1000000000. Failed to get this system's current limit.
WARNING: To avoid network timeouts, net.core.somaxconn should be at least 1024. Failed to get this system's current limit.
WARNING: To avoid crashes on OOM conditions, vm.min_free_kbytes should be at least 81289. Failed to get this system's current limit.
Please visit https://docs.singlestore.com/memsqlctl-redir/linux-ulimit-settings for more information.
✓ Started Leaf nodes on singlestoreupgrade-test-s2_node-1 (1/2)
memsqlctl output:
WARNING: The NICE limit should be at most -10. This system is currently configured with the limit set to 20.
WARNING: To avoid memory errors, vm.max_map_count should be at least 1000000000. Failed to get this system's current limit.
WARNING: To avoid network timeouts, net.core.somaxconn should be at least 1024. Failed to get this system's current limit.
WARNING: To avoid crashes on OOM conditions, vm.min_free_kbytes should be at least 81289. Failed to get this system's current limit.
Please visit https://docs.singlestore.com/memsqlctl-redir/linux-ulimit-settings for more information.
✓ Started Leaf nodes on singlestoreupgrade-test-s2_node-2 (2/2)
✓ Successfully started Leaf nodes on 2 hosts
✓ Successfully connected to Leaf nodes
memsqlctl output:
WARNING: The NICE limit should be at most -10. This system is currently configured with the limit set to 20.
WARNING: To avoid memory errors, vm.max_map_count should be at least 1000000000. Failed to get this system's current limit.
WARNING: To avoid network timeouts, net.core.somaxconn should be at least 1024. Failed to get this system's current limit.
WARNING: To avoid crashes on OOM conditions, vm.min_free_kbytes should be at least 81289. Failed to get this system's current limit.
Please visit https://docs.singlestore.com/memsqlctl-redir/linux-ulimit-settings for more information.
✓ Started Master node on singlestoreupgrade-test-s2_master-1 (1/1)
✓ Successfully started Master node on 1 host
✓ Successfully connected to Master node
✓ Snapshots completed
✓ Successfully uninstalled from host singlestoreupgrade-test-s2_master-1 (1/3)
✘ Failed to uninstall from host singlestoreupgrade-test-s2_node-1 (2/3)
✓ Successfully uninstalled from host singlestoreupgrade-test-s2_node-2 (3/3)
✓ Successfully uninstalled singlestoredb-server 8.0.32 from 2 hosts
✓ Set auto_attach to ON
✓ Set leaf_failure_detection to ON
```
* There might be warnings. Also a minor fraction of sub-tasks might fail but for this upgrade tests these can be ignored
* Verify that all nodes are online and up to date `./sdb-admin list-nodes`
```
+------------+--------+-------------------------------------+------+---------------+--------------+---------+----------------+--------------------+--------------+
| MemSQL ID  |  Role  |                Host                 | Port | Process State | Connectable? | Version | Recovery State | Availability Group | Bind Address |
+------------+--------+-------------------------------------+------+---------------+--------------+---------+----------------+--------------------+--------------+
| A90123FBB8 | Master | singlestoreupgrade-test-s2_master-1 | 3306 | Running       | True         | 8.5.11  | Online         |                    | 0.0.0.0      |
| E99703A958 | Leaf   | singlestoreupgrade-test-s2_node-1   | 3306 | Running       | True         | 8.5.11  | Online         | 1                  | 0.0.0.0      |
| DFCCA6E004 | Leaf   | singlestoreupgrade-test-s2_node-2   | 3306 | Running       | True         | 8.5.11  | Online         | 1                  | 0.0.0.0      |
+------------+--------+-------------------------------------+------+---------------+--------------+---------+----------------+--------------------+--------------+
```
## Upgrade Steps
Here is an outline of the Upgrade steps:
* Upgrade the toolbox
  * via package manager - `sudo yum install singlestoredb-toolbox -y`
  * via rpm - `sudo yum install /path/to/singlestoredb-toolbox.rpm -y`
  * via tarball (this repo)
    * get the download location URLs as JSON - `curl -O https://release.memsql.com/production/index/memsqltoolbox/latest.json`
    * check `.packages."memsql-toolbox-tar".Path'` in the JSON
    * download file e.g. - `curl -o memsqltoolbox.tar.gz https://release.memsql.com/production/tar/x86_64/singlestoredb-toolbox-1.17.9-2915071185.x86_64.tar.gz`
    * unpack the tarball - `tar xvzf memsqltoolbox.tar.gz -C <TARGET_FOLDER>`
  * Using the toolbox
    * if you installed the toolbox via rpm/package manager the toolbox commands are in your $PATH
    * if you installed the toolbox via tarball the toolbox is not in you $PATH you can either:
      * add it to you path
      * `cd` into the directory and use the commands from there e.g. `./sdb-deploy ...`
    * following it is assumed that the toolbox has been added to the $PATH
* Upgrade SingleStoreDB
  * Pre-Check the Cluster `sdb-deploy upgrade --precheck-only`
  * upgrade via online install (needs internet connectivity)
    * `sdb-deploy upgrade --version 8.5`
  * upgrade offline
    * get the download location URLs as JSON - `curl -O https://release.memsql.com/production/index/singlestoredbserver/latest.json`
    * check `.packages."singlestoredb-server-tar".Path'` in the JSON
    * download file e.g. - `curl -o singlestoredb-server-tar.tar.gz https://release.memsql.com/production/tar/x86_64/singlestoredb-server-8.5.11-b906f298a2.x86_64.tar.gz`
    * deploy the upgrade - `sdb-deploy upgrade --file-path singlestoredb-server-tar.tar.gz`
## Further reading and links
[SingleStore Docs - Upgrade - 8.5](https://docs.singlestore.com/db/v8.5/user-and-cluster-administration/upgrade-or-uninstall-singlestore/upgrade-singlestore/)