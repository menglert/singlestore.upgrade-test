#!/bin/bash

cd singlestoredb-toolbox
./sdb-deploy setup-cluster --temp-dir /opt/singlestore/tmp/ --cluster-file ../s2_cluster.yaml -v=3 --ssh-max-sessions 3 --yes 2> setup-cluster.log